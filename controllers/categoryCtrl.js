const Category = require('../models/categoryModel')
const Products = require('../models/productModel')

const categoryCtrl = {
    getCategories: async(req, res) =>{
        try {
            const categories = await Category.find()
            res.json(categories)
        } catch (err) {
            return res.status(500).json(err.message)
        }
    },
    createCategory: async (req, res) =>{
        try {
            // 1 === admin
            const {name} = req.body;
            const category = await Category.findOne({name})
            if(category) return res.status(400).json("Category Already Exists")

            const newCategory = new Category({name})

            await newCategory.save()
            res.json("Category Created Successfully")
        } catch (err) {
            return res.status(500).json(err.message)
        }
    },
    deleteCategory: async(req, res) =>{
        try {
            const products = await Products.findOne({category: req.params.id})
            if(products) return res.status(400).json("Please delete all products with a relationship."
            )

            await Category.findByIdAndDelete(req.params.id)
            res.json("Category Deleted Successfully")
        } catch (err) {
            return res.status(500).json(err.message)
        }
    },
    updateCategory: async(req, res) =>{
        try {
            const {name} = req.body;
            await Category.findOneAndUpdate({_id: req.params.id}, {name})

            res.json("Category Updated Successfully")
        } catch (err) {
            return res.status(500).json(err.message)
        }
    }
}


module.exports = categoryCtrl